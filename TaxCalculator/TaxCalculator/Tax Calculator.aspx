﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Tax Calculator.aspx.cs" Inherits="TaxCalculator.Tax_Calculator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #moneyval {
            height: 46px;
            width: 460px;
            color: #FFFFCC;
            background-color: #0033CC;
        }
        #taxpercent {
            height: 44px;
            width: 463px;
            color: #FFFFFF;
            background-color: #006600;
        }
        .auto-style1 {
            color: #FF0000;
            font-size: xx-large;
        }
    </style>
</head>
<body style="font-size: x-large">
    <form id="form1" runat="server" oninput="taxval.value=(parseFloat(moneyval.value)*parseFloat(taxpercent.value)/100)">
        <div>
            Amount of money:<br />
            <input type="number" id="moneyval" value="" />
            <br />
            Tax Percentage rate:
            <br />
            <input type="number" id="taxpercent" value="" />
            <br />
            <br />
            <span>The value of the tax you need to pay is <span class="auto-style1">$</span></span>
            <output name="taxval" for="moneyval taxpercent"/>
        </div>
    </form>
</body>
</html>
